---
title: 'EDA: result, discussion and conclusion'
author: "Stijn Arends"
date: "9/27/2019"
output: pdf_document
---

```{r include=FALSE}
library("tinytex")
library("scales")
library("kableExtra")
library("dplyr")
library("gridExtra")
library("ggplot2")
library("ggridges")
library("tidyr")
library("plyr")
library("ggcorrplot")
library("ggdendro")
library("grid")
library("reshape2")
library("ggfortify")
```

```{r include = FALSE}
knitr::opts_chunk$set(cache=TRUE)
```


```{r include = FALSE}
heart_disease_database <- read.table(file = "/homes/sarends/jaar_3/Thema_9/thema09_repo/processed.cleveland.data", sep = ",")

names <- c("age","sex","cp","trestbps","chol","fbs","restecg","thalach","exang",
           "oldpeak","slope","ca","thal","dxlevel")

colnames(heart_disease_database) <- names

```

```{r include=FALSE}
# Creating a pleasent theme that will be used by multiple plots
theme_plot <- theme(panel.background = element_blank(),panel.border=element_rect(fill=NA),
        panel.grid.major = element_blank(),panel.grid.minor = element_blank(),strip.background=element_blank(),
        axis.text.x=element_text(colour="black"),axis.text.y=element_text(colour="black"),
        axis.ticks=element_line(colour="black"),plot.margin=unit(c(1,1,1,1),"line"))
```
# Results

Before a dataset is able to be used for the process of machine learning it must go through a process called exploratory data analysis(EDA).
EDA is an approach to analyze a data set, to summarize the main characteristics and to tell how the data is constructed. 

The first thing that has to be done when performing an EDA is taking a look at the original data.
Looking at figure \ref{fig:one} it is clear to see that there are missing values involved.
These missing values have been replaced with the value -9 in order to prepare the data for the process of machine learning.

Once the data has been loaded in and the structure of the data has been checked for unusual encoding of missing values and the data type of the columns have been checked. It is time to explore some variables, this step gives a first hint on how the data is distributed and if there is something wrong with the data.
Using a boxplot of the whole data the distribution of the attributes is visualized(figure \ref{fig:two}). In figure \ref{fig:two} all different attributes are displayed on the x-axis and on the y-axis are their corresponding values. Looking at this boxplot it still isn't clear how all the different attributes are distributed because the attributes range across orders of magnitude. That is why the data was seperated into two groups and looked at individually. The first group displays the attributes age, trestbps, chol and thalach(figure \ref{fig:three}). In figure \ref{fig:three} the four attributes are displayed on the x-axis and their corresponding values on the y-axis. While looking at this boxplot it is clear to see that the chol attribute has a few outliers and that the trestbps also has a few outliers, however these seemed to be less spread out. The second group displays the attributes sex, cp, fbs, restecg, exang, oldpeak, slope, ca, thal and dxlevel(figure \ref{fig:four}). In figure \ref{fig:four} the distributions of these attributes are shown, where the attributes are displayed on the x-axis and their values on the y-axis. This figure shows that the oldpeak attribute consist of a few outliers and that the fbs attribute has little variation. 

To get a more clear overview over the distribution of each attribute in the data, multiple density ridges plots have been created(figure \ref{fig:five}, figure \ref{fig:six}). These figures show all the attributes lined beneath each other where their values are displayed on the x-axis and their names and density are displayed on the y-axis. The density plot is furthermore used to see wheter an attribute will be informative in the modeling process by splitting the data on the class label(\ref{fig:seven}). In figure \ref{fig:seven} the attribute chol is split on the class label(dxlevel). The values of chol are coverd on the x-axis and the density of those values are displayed on the y-axis. This figures shows whether a certain value might correspond with a certain value of the class label. 

It is important to know how the class label is distributed, this is shown in figure \ref{fig:eight}}. This figure shows that by far the most peaople have no disease(value 0).

Now that there is a clear view of the distribution of the data the relationship of the attributes can be explored.
This can be done in different ways. In figures \ref{fig:nine}, \ref{fig:ten} and \ref{fig:eleven} the relationship of different attributes are visualized using scatterplots. In figure \ref{fig:nine} the relationship of the attributes thalach and ca are shown were the attribute thalach covers the x-axis and the ca attribute covers the y-axis. The regression line goes from top-left to mid-right, this indicates that the attributes thalach and ca are negatively correlated to eachother. In figure \ref{fig:ten} the relationship of the attributes cp and chol are shown where the attribute cp covers the x-axis and the chol attribute covers the y-axis. The regression line increases slightly from bottom-left to bottom-right, meaning that the attributes cp and chol almost have no correlation with eachother. In figure \ref{fig:eleven} the relationship of the attributes oldpeak and slope are shown where the attribute oldpeak covers the x-axis and the slope attribute covers the y-axis. The regression line increass from bottom-left to top-right. This means that these two attributes are correlated to eachother. Another way to explore the relationship of the attributes is with a correlation matrix(figure \ref{fig:twelve}). This figure shows every possible relationship between attributes.\newpage

Now that the relationship between each attribute has been determined it is time to cluster the data. Clustering of the data can be done by different ways. One of them is the principal component analysis(PCA). Shown in figure \ref{fig:thirteen} is a PCA plot of the data. The axis are ranked in order of importance. The first prinicpal component axis(PC1) shows the most varaiance and is therefore the most important and the second prinicpal component axis(PC2) shows the second most variance. In the PCA plot the attributes that are correlated to each other will cluster together. This figure shows that the attributes chol and thalach are not correlated with any other attributesand that the rest is somewhat correlated to each other.








# Discussion

The dataset consist of a sufficient amount of instances and attributes. Some of these attributes cotain missing values. These missing values have been replaced by the value -9 in order for the dataset to be ready to be used in machine learning experiments.
With the help of functions like structure and summary the overall structure of the data was determined. Those functions showed that the data was not corrupted. By creating boxplots of the data it was clear to see that some attributes contained outliers. The instances containing these outliers were removed from the dataset. By creating boxplots and density plots the distribution of the attributes were explored. 
From these plots it was clear to see that most of the attributes distributions were fine, however the density and boxplots also showed that the fbs attribute showed little variation. That is why the fbs attribute has been removed from the dataset. With the help of scatterplots and a correlation matrix the relationships of the attributes were discovered. These scatter plots and the correlation matrix showed that most of the attributes were independent of each other. Although most of the attribute were not correlated with each other at all some of them showed a little correlation with each other. The correlation of those attributes were not high enough for them to be removed or combined into a single value. Looking at data it is clear to see that the attributes range across orders of magnitude. That is why some of these attributes were log(2) transformed to eliminate that effect. 


# Conclusion

In conclusion I think that I have obtained the best possible 'clean' version of the dataset that is suitible for the machine learning process. Because the attributes do not range across order of magnitude anymore, the data is not corrupted, the instances with outliers and the attribute that showed little variation have been removed and the attributes are not highly correlated with each other.

The dataset could have been improved by adding more instances. By adding more instances the class distribution would change and maybe in a way were the class labels would be more evenely represented. Another way to improve the dataset is by adding other attributes. For example: smoking, excessive use of alcohol or caffeine and irregular heartbeat.


```{r echo =FALSE, fig.cap = "A barchart showing the presence of missing values \\label{fig:one}"}
set.seed(1)
missing.data <- data.frame(ca = heart_disease_database$ca)
missing.data.barplot <- missing.data %>% 
  dplyr::group_by(ca) %>%
  dplyr::summarise(count = length(ca))

ggplot(data = missing.data.barplot, aes(x = ca, y = count, fill = ca)) + 
  geom_bar(stat = 'identity', fill = "steelblue") +
  geom_text(aes(label = count),  vjust=0.0, color="black", size=3.5) +
  labs(title = "Barchart of the attribute ca",
       x = "ca values") + 
  theme_plot
```


```{r echo = FALSE}
# Looking at the structure of the data

# Reloading the data because of the "?" value
heart_disease_database <- read.table(file = "/homes/sarends/jaar_3/Thema_9/thema09_repo/processed.cleveland.data", sep = ",", na.strings = "?")

names <- c("age","sex","cp","trestbps","chol","fbs","restecg","thalach","exang",
           "oldpeak","slope","ca","thal","dxlevel")

colnames(heart_disease_database) <- names

# Replace the NAs with -9
heart_disease_database[is.na(heart_disease_database)] <- -9
```


```{r echo =FALSE, fig.cap = "A boxplot of the complete dataset \\label{fig:two}"}

collapsed_heartDS_data <- gather(heart_disease_database)

ggplot(collapsed_heartDS_data,
       aes(x = collapsed_heartDS_data$key, y = collapsed_heartDS_data$value,
           fill = key)) +
  geom_boxplot() +
  labs(x = "Attributes", y = "Value", title = "Boxplot of all the attributes") +
  scale_fill_discrete(name = "Attribute") +
  theme_plot

```


```{r echo =FALSE, fig.cap = " A boxplot showing the distribution of the value of the attributes age,trestbps,chol and thalach \\label{fig:three}"}
collapsed_heartDS_data <- gather(heart_disease_database)

# Creating a subset of the original data frame to take a closer look at some of the attributes
collapsed_heartDS_ATCT_data <- collapsed_heartDS_data[(collapsed_heartDS_data$key %in% c("age","trestbps","chol","thalach")),]

# Check the str of the new data
#str(collapsed_heartDS_ATCT_data)

# Rewrite the levels of the attribute in the data frame.
collapsed_heartDS_ATCT_data$key <- factor(collapsed_heartDS_ATCT_data$key,
                                           levels = c("age","trestbps","chol","thalach"))

ggplot(collapsed_heartDS_ATCT_data,
       aes(x = collapsed_heartDS_ATCT_data$key, y = collapsed_heartDS_ATCT_data$value,
           fill = key)) +
  geom_boxplot() +
  labs(x = "Attributes", y = "Value", title = "Boxplot of the attributes age, trestbps, chol and thalach") +
  scale_fill_discrete(name = "Attribute", labels = c("age(years)","trestbps(mm/Hg)","chol(mg/dl)","thalach(max heart rate)")) +
  theme_plot
```


```{r echo=FALSE, fig.cap = " A boxplot showing the distribution of the value of the attributes sex, cp, fbs, restecg, exang, oldpeak, slobe, ca, thal and dxlevel \\label{fig:four}"}

collapsed_heartDS_g2 <- collapsed_heartDS_data[(collapsed_heartDS_data$key %in% c("sex","cp","fbs",
                                                                                       "restecg",
                                                                                       "exang",
                                                                                       "oldpeak","slope",
                                                                                       "ca","thal", "dxlevel"
                                                                                       )),]

ggplot(collapsed_heartDS_g2,
       aes(x = collapsed_heartDS_g2$key, y = collapsed_heartDS_g2$value,
           fill = key)) +
  geom_boxplot() +
  labs(x = "Attributes", y = "Value", title = "Boxplot of the attributes sex, cp, fbs, restecg, exang, 
       oldpeak, slope, ca, thal and dxlevel") +
  scale_fill_discrete(name = "Attribute") +
  theme_plot
```


```{r include = FALSE}
collapsed_heartDS_data_g1 <- collapsed_heartDS_data[(collapsed_heartDS_data$key %in% c("age", 
                                                                                       "trestbps","chol",
                                                                                       "thalach")),]
collapsed_heartDS_data_g1$key <- factor(collapsed_heartDS_data_g1$key,
                                           levels = c("age","trestbps","chol","thalach"))


collapsed_heartDS_data_g2 <- collapsed_heartDS_data[(collapsed_heartDS_data$key %in% c("sex","cp","fbs",
                                                                                       "restecg",
                                                                                       "exang",
                                                                                       "oldpeak","slope",
                                                                                       "ca","thal", "dxlevel"
                                                                                       )),]
collapsed_heartDS_data_g2$key <- factor(collapsed_heartDS_data_g2$key,
                                        levels = c("sex","cp","fbs",
                                                  "restecg",
                                                  "exang",
                                                  "oldpeak","slope",
                                                  "ca","thal", "dxlevel"))
```


```{r echo=FALSE, fig.cap = " A density ridge plot showing the distribution of the attributes age, trestbps, chol and thalach \\label{fig:five}"}

ggplot(collapsed_heartDS_data_g1, aes(x = collapsed_heartDS_data_g1$value, y = collapsed_heartDS_data_g1$key,
                                      fill = collapsed_heartDS_data_g1$key)) +
  geom_density_ridges() +
  labs(x ="Value", y ="Attributes") +
  scale_fill_discrete(name = "Attribute", labels = c("age(years)","trestbps(mm/Hg)",
                                                     "chol(mg/dl)","thalach(max heart rate)")) +
  ggtitle("Density ridge plot") +
  theme_plot
```


```{r echo=FALSE, fig.cap = " A density ridges plot showing the distribution of the attributes sex, cp, fbs, restecg, exang, oldpeak, slobe, ca, thal and dxlevel \\label{fig:six}"}
ggplot(collapsed_heartDS_data_g2, aes(x = collapsed_heartDS_data_g2$value, y = collapsed_heartDS_data_g2$key,
                                      fill = collapsed_heartDS_data_g2$key)) +
  geom_density_ridges() +
  labs(x ="Value", y ="Attributes") + 
  scale_fill_discrete(name = "Attribute", labels = c("sex","cp","fbs","restecg","exang",
                                                     "oldpeak","slope","ca","thal","dxlevel")) + 
  ggtitle("Density ridge plot") +
  theme_plot
```


```{r echo=FALSE, fig.cap = " A density plot of the attribute chol split on the class label \\label{fig:seven}"}
# Factorize the class attribute so it can be used to split the data
density.class.distribution.data <- heart_disease_database
density.class.distribution.data$dxlevel <- factor(density.class.distribution.data$dxlevel,
                               levels = c(0,1,2,3,4))

# Plot the attributes density split on the class attribute
ggplot(density.class.distribution.data, aes(x = density.class.distribution.data$chol, 
                                                    colour = dxlevel)) + 
  geom_density() +
  labs(x = "Value", y = "Densiy", title = "Density plot of chol split on the class label") +
  scale_color_discrete(name = "Class label: dxlevel", labels = c("0 = no disease","1 = mild disease","2 = moderate disease",
                                    "3 = acute disease","4 = severe disease")) +
  theme_plot

```


```{r echo=FALSE, fig.cap = " A Barchart showing the distribution of the class attribute \\label{fig:eight}"}
class.attribute <- collapsed_heartDS_data[(collapsed_heartDS_data$key %in% c("dxlevel")),]

set.seed(1)
class.attr.df <- data.frame(dxlevel = heart_disease_database$dxlevel)
class.attr.barplot <- class.attr.df %>% 
  dplyr::group_by(dxlevel) %>%
  dplyr::summarise(count = length(dxlevel))

ggplot(data = class.attr.barplot, aes(x = dxlevel, y = count, fill = dxlevel)) + 
  geom_bar(stat = 'identity', fill = "steelblue") +
  geom_text(aes(label = count),  vjust=1.6, color="white", size=3.5) +
  labs(title = "Barchart of the class attribute",
       x = "dxlevel values") +
  theme_plot +
  theme(plot.caption = element_text(hjust = 0))
```


```{r echo=FALSE, fig.cap = " A scatter plot showing the relationship between the attributes thalach and ca \\label{fig:nine}"}
ggplot(heart_disease_database, aes(x = heart_disease_database$thalach,
                                   y = heart_disease_database$ca,
                                   color = heart_disease_database$dxlevel)) +
  geom_point(alpha = 0.5) +
  geom_jitter() +
  geom_smooth(method = lm, se = FALSE) +
  labs(x = "Attribute: thalach(bps)", y = "Attribute: ca(# major bloodvessels)", 
       title = "Scatterplot showing the relationship between the thalach and ca") +
  scale_color_continuous(name = "dxlevels: presence heart disease", 
                         labels = c("0 = no disease","1 = mild disease","2 = moderate disease",
                                    "3 = acute disease","4 = severe disease")) +
  theme_plot
```


```{r echo=FALSE, fig.cap = " A scatter plot showing the relationship between the attributes cp and chol \\label{fig:ten}"}
ggplot(heart_disease_database, aes(x = heart_disease_database$cp,
                                   y = heart_disease_database$chol,
                                   color = heart_disease_database$dxlevel)) +
  geom_point(alpha = 0.5) +
  geom_jitter() +
  geom_smooth(method = lm, se = FALSE) +
  labs(x = "Attribute: cp(chestpain)", y = "Attribute: chol(mg/dl)", 
       title = "Scatterplot showing the relationship between the cp and chol") +
  scale_color_continuous(name = "dxlevels: presence heart disease", 
                         labels = c("0 = no disease","1 = mild disease","2 = moderate disease",
                                    "3 = acute disease","4 = severe disease")) +
  theme_plot

```


```{r echo=FALSE, fig.cap = " A scatter plot showing the relationship between the attributes oldpeak and slope \\label{fig:eleven}"}
ggplot(heart_disease_database, aes(x = heart_disease_database$oldpeak,
                                   y = heart_disease_database$slope,
                                   color = heart_disease_database$dxlevel)) +
  geom_point(alpha = 0.5) +
  geom_jitter() +
  geom_smooth(method = lm, se = FALSE) +
  labs(x = "Attribute: oldpeak(ST depression induced 
       by exercise relative to rest)", 
       y = "Attribute: slope(the slope of the 
       peak exercise ST segment)", 
       title = "Scatterplot showing the relationship between the oldpeak 
       and slope attributes") +
  scale_color_continuous(name = "dxlevels: presence heart disease", 
                         labels = c("0 = no disease","1 = mild disease","2 = moderate disease",
                                    "3 = acute disease","4 = severe disease")) +
  theme_plot
```


```{r echo = FALSE, fig.cap = " A correlation matrix showing the relationship of each attribute\\label{fig:twelve}"}
ggcorrplot(corr = cor(heart_disease_database), outline.color = "black", lab = TRUE,
           lab_size = 2.5) +
  labs(title = "Correlation matrix of the dataset")
```


```{r echo = FALSE, fig.cap = " A PCA plot showing the clustering of the attributes \\label{fig:thirteen}"}
# Create pca data
df_PCA <- prcomp(heart_disease_database)

# Create new data frame
df_out <- as.data.frame(df_PCA$x)

# Calculate the percentages so it can be displayed in the plot
percentage <- round(df_PCA$sdev / sum(df_PCA$sdev) * 100, 2)
percentage <- paste(colnames(df_out), "(", paste(as.character(percentage),"%", ")", sep = ""))

# Create a theme variable that holds the theme() atribute of the plot
theme_plot <- theme(panel.background = element_blank(),panel.border=element_rect(fill=NA),
        panel.grid.major = element_blank(),panel.grid.minor = element_blank(),strip.background=element_blank(),
        axis.text.x=element_text(colour="black"),axis.text.y=element_text(colour="black"),
        axis.ticks=element_line(colour="black"),plot.margin=unit(c(1,1,1,1),"line"))

# Hide the plot
invisible(ggplot(df_out, aes(df_out$PC1, df_out$PC2, label = row.names(heart_disease_database))) +
  geom_point() +
  geom_text(size = 3) +
  theme_plot +
  labs(x = percentage[1], y = percentage[2]))


# Plot the attributes
df_out_attr <- as.data.frame(df_PCA$rotation)

# Change the rownames to the attribute names
df_out_attr$attributes <- row.names(df_out_attr)

ggplot(df_out_attr, aes(x = df_out_attr$PC1, y = df_out_attr$PC2, 
                        label = df_out_attr$attributes, color = df_out_attr$attributes)) +
  geom_point() + 
  geom_text(size = 3) +
  labs(x = percentage[1], y = percentage[2], title = "PCA of all attributes") +
  scale_color_discrete(name = "Attributes") +
  theme_plot
```



